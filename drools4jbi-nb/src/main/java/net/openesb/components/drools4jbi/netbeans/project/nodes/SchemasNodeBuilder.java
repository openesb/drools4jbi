/*
 * @(#)SchemasNodeBuilder.java        $Revision: 1.6 $ $Date: 2008/11/12 08:26:25 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package net.openesb.components.drools4jbi.netbeans.project.nodes;

import net.openesb.components.drools4jbi.netbeans.project.directory.DirectoryManager;
import net.openesb.components.drools4jbi.netbeans.project.fileimport.FileType;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.PrivilegedTemplates;
import org.openide.util.ImageUtilities;

import java.awt.*;

/**
 * Builder for the "XML Schema Files" node.
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.6 $ $Date: 2008/11/12 08:26:25 $
 * 
 * @since 0.3
 */
public final class SchemasNodeBuilder extends DirectoryNodeBuilder {

    private static final String SCHEMAS_NODE_DISPLAY_NAME = "XML Schema Files";
    
    @StaticResource()
    public static final String SCHEMAS_BADGE_URL = "net/openesb/components/drools4jbi/netbeans/schemasBadge.png";
    private static final Image SCHEMAS_BADGE = ImageUtilities.loadImage(SCHEMAS_BADGE_URL);

    public SchemasNodeBuilder(Project project) {
        super(project.getLookup().lookup(DirectoryManager.class).getSchemasDirectory());
        
        displayName(SCHEMAS_NODE_DISPLAY_NAME);
        badge(SCHEMAS_BADGE);
        addExtension("xsd");
        supportImport(FileType.XML_SCHEMA, project.getLookup().lookup(DirectoryManager.class));
        showSubfolders();
        privilegedTemplates(new SchemasNodePrivilegedTemplates());
    }
    
    private static class SchemasNodePrivilegedTemplates implements PrivilegedTemplates {

        private static final String[] PRIVILEGED_TEMPLATES = new String[] {
            "Templates/XML/XmlSchema.xsd",
            "Templates/Other/Folder",
        };

        public String[] getPrivilegedTemplates() {
            return PRIVILEGED_TEMPLATES;
        }
    }
}
