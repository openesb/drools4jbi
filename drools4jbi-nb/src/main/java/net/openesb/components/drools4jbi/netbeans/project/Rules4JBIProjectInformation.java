/*
 * @(#)Rules4JBIProjectInformation.java        $Revision: 1.1.1.1 $ $Date: 2008/06/30 08:53:20 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package net.openesb.components.drools4jbi.netbeans.project;

import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.openide.util.ImageUtilities;

import javax.swing.*;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.1.1.1 $ $Date: 2008/06/30 08:53:20 $
 * 
 * @see org.netbeans.api.project.ProjectInformation
 * @since 0.1
 */
public final class Rules4JBIProjectInformation implements ProjectInformation {
    
    private final Project project;

    @StaticResource()
    public static final String RULES4JBI_ICON = "net/openesb/components/drools4jbi/netbeans/projectIcon.png";
    
    public Rules4JBIProjectInformation(final Project project) {
        this.project = project;
    }

    @Override
    public Project getProject() {
        return project;
    }
    
    @Override
    public String getName() {
        return project.getProjectDirectory().getName();
    }
    
    @Override
    public String getDisplayName() {
        return project.getProjectDirectory().getName();
    }

    @Override
    public Icon getIcon() {
        return new ImageIcon(ImageUtilities.loadImage(RULES4JBI_ICON));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        //do nothing; name, displayName, and projectIcon won't change
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        //do nothing; name, displayName, and projectIcon won't change
    }
}
