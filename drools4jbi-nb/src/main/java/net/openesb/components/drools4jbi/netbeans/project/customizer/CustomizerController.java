/*
 * @(#)CustomizerController.java        $Revision: 1.4 $ $Date: 2008/12/17 23:21:35 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package net.openesb.components.drools4jbi.netbeans.project.customizer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.NbBundle;

import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;

import net.openesb.components.drools4jbi.shared.GlobalConstants;
import net.openesb.components.drools4jbi.shared.config.Configuration;

import net.openesb.components.drools4jbi.netbeans.project.directory.DirectoryManager;
import net.openesb.components.drools4jbi.netbeans.project.fileimport.FileType;
import net.openesb.components.drools4jbi.netbeans.project.fileimport.ImportFileActionHandler;
import net.openesb.components.drools4jbi.netbeans.project.nodes.RulesEngineNodeBuilder;
import net.openesb.components.drools4jbi.netbeans.project.nodes.RulesNodeBuilder;
import net.openesb.components.drools4jbi.netbeans.util.RulesEngineProvidersFinder;
import net.openesb.components.drools4jbi.netbeans.util.chooser.FileChooserDialogDescriptor;
import net.openesb.components.drools4jbi.netbeans.util.chooser.FileChooserPanel;
import org.apache.commons.io.IOUtils;
import org.openide.util.Exceptions;

/**
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.4 $ $Date: 2008/12/17 23:21:35 $
 *
 * @since 0.1
 */
class CustomizerController {

    private static final Logger logger = Logger.getLogger(CustomizerController.class.getName());

    private final GeneralPanel generalPanel;

    private final RulesEnginePanel rulesEnginePanel;

    private final Project project;

    private final Configuration configuration;

    private final DirectoryManager directoryManager;

    //   private final Node rulesEngineNode;
    CustomizerController(Project project) {
        this.project = project;

        directoryManager = project.getLookup().lookup(DirectoryManager.class);

        configuration = project.getLookup().lookup(Configuration.class);
        logger.fine("Retrieved project configuration: " + configuration);

        //rulesEngineNode = new RulesEngineNodeBuilder(project).createNode();
        generalPanel = new GeneralPanel();
        rulesEnginePanel = new RulesEnginePanel(this);

        generalPanel.setProjectFolder(FileUtil.getFileDisplayName(project.getProjectDirectory()));
        generalPanel.setServiceEngineType(GlobalConstants.SERVICE_ENGINE_NAME);

        // rulesEnginePanel.selectProvider(configuration.getRuleServiceProvider());
        // rulesEnginePanel.selectProviderClass(configuration.getRuleServiceProviderClass());
        rulesEnginePanel.setRulesetFileName(configuration.getRulesetFile());
        rulesEnginePanel.setRuleflowFileName(configuration.getRuleflowFile());
        rulesEnginePanel.setProcessName(configuration.getProcessName());

    }

    GeneralPanel getGeneralPanel() {
        return generalPanel;
    }

    RulesEnginePanel getRulesEnginePanel() {
        return rulesEnginePanel;
    }

    void handleBrowseButtonPressedRuleFile() {
        logger.finer("Browse button pressed");

        String title = NbBundle.getMessage(CustomizerController.class,
                "CustomizerController.ruleFileChooserDialog.title");

        final Node rootNode = new RulesNodeBuilder(project).createNode();

        FileChooserDialogDescriptor dialogDescriptor
                = new FileChooserDialogDescriptor(new FileChooserPanel(rootNode, title), title);

        String currentRulesetFileName = rulesEnginePanel.getRulesetFileName();

        logger.finer("Current ruleset file: " + currentRulesetFileName);

        Node currentNode = null;

        if (!currentRulesetFileName.equals("")) {
            Node[] rootChildNodes = rootNode.getChildren().getNodes(true);
            logger.finer("Searching following nodes: " + Arrays.toString(rootChildNodes));

            for (Node rootChild : rootChildNodes) {
                if (currentRulesetFileName.equals(rootChild.getDisplayName())) {
                    currentNode = rootChild;
                    break;
                }
            }
        }

        dialogDescriptor.clearSelection();

        if (currentNode != null) {
            dialogDescriptor.selectNode(currentNode);
        }

        Object result = DialogDisplayer.getDefault().notify(dialogDescriptor);

        if (DialogDescriptor.OK_OPTION.equals(result)) {
            logger.fine("Rule file selection confirmed");

            FileObject selectedFile = dialogDescriptor.getSelectedFile();

            if (selectedFile == null) {
                throw new AssertionError("Selected file cannot be null");
            }

            logger.fine("Selected rule file: " + selectedFile.getNameExt());

            rulesEnginePanel.setRulesetFileName(selectedFile.getNameExt());
        }
    }

    void resetRuleFlowFileandProcessID() {
        rulesEnginePanel.setRuleflowFileName("");
        rulesEnginePanel.setProcessName("");
    }

    void resetRuleFile() {
        rulesEnginePanel.setRulesetFileName("");
        //rulesEnginePanel.setr
    }

    void handleBrowseButtonPressedRuleFlowFile() {
        logger.finer("Browse button pressed");

        String title = NbBundle.getMessage(CustomizerController.class,
                "CustomizerController.ruleFileChooserDialog.title1");

        final Node rootNode = new RulesNodeBuilder(project).createNode();

        FileChooserDialogDescriptor dialogDescriptor
                = new FileChooserDialogDescriptor(new FileChooserPanel(rootNode, title), title);

        String currentRuleflowFileName = rulesEnginePanel.getRuleflowFileName();

        logger.finer("Current ruleflow file: " + currentRuleflowFileName);

        Node currentNode = null;

        if (!currentRuleflowFileName.equals("")) {
            Node[] rootChildNodes = rootNode.getChildren().getNodes(true);
            logger.finer("Searching following nodes: " + Arrays.toString(rootChildNodes));

            for (Node rootChild : rootChildNodes) {
                if (currentRuleflowFileName.equals(rootChild.getDisplayName())) {
                    currentNode = rootChild;
                    break;
                }
            }
        }

        dialogDescriptor.clearSelection();

        if (currentNode != null) {
            dialogDescriptor.selectNode(currentNode);
        }

        Object result = DialogDisplayer.getDefault().notify(dialogDescriptor);

        if (DialogDescriptor.OK_OPTION.equals(result)) {
            logger.fine("Ruleflow  selection confirmed");

            FileObject selectedFile = dialogDescriptor.getSelectedFile();

            if (selectedFile == null) {
                throw new AssertionError("Selected file cannot be null");
            }

            logger.fine("Selected ruleflow file: " + selectedFile.getNameExt());

            rulesEnginePanel.setRuleflowFileName(selectedFile.getNameExt());
            try {
                String fileContent = IOUtils.toString(selectedFile.getInputStream());
                String processID = getProcessID(fileContent);
                if (processID != null && processID.length() > 0) {
                    rulesEnginePanel.setProcessName(processID);
                }
            } catch (FileNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private String getProcessID(String fileContent) {
        String processID = null;
        String searchElement = "bpmn2:process id=\"";
        int startIndex = 0;
        int stopIndex = 0;
        startIndex = fileContent.indexOf(searchElement) + searchElement.length();
        stopIndex = fileContent.indexOf("\"", startIndex + 1);
        processID = fileContent.substring(startIndex, stopIndex);

        return processID;
    }

    void handleOkButtonPressed() {
        logger.fine("Saving configuration changes");

        configuration.setRulesetFile(rulesEnginePanel.getRulesetFileName());
        configuration.setRuleflowFile(rulesEnginePanel.getRuleflowFileName());
        configuration.setProcessName(rulesEnginePanel.getProcessName());
    }

    void handleWindowClosing() {
        logger.fine("Window closing");
    }

    void handleWindowClosed() {
        logger.fine("Window closed");
    }
}
