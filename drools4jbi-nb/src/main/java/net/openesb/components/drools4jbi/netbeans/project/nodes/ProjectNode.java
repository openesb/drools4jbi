/*
 * @(#)ProjectNode.java        $Revision: 1.3 $ $Date: 2008/11/12 08:26:25 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package net.openesb.components.drools4jbi.netbeans.project.nodes;

import net.openesb.components.drools4jbi.netbeans.project.actions.ActionsFactory;
import net.openesb.components.drools4jbi.netbeans.project.directory.DirectoryManager;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.ProjectSensitiveActions;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * Root node of the project, shown in the project explorer view.
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.3 $ $Date: 2008/11/12 08:26:25 $
 * 
 * @since 0.1
 */
public class ProjectNode extends AbstractNode {
    
    @StaticResource()
    public static final String RULES4JBI_ICON = "net/openesb/components/drools4jbi/netbeans/projectIcon.png";
    
    public enum Type { RULES, SOURCES, SCHEMAS, LIBRARIES, DESCRIPTIONS}

    private ResourceBundle resourceBundle = NbBundle.getBundle(ProjectNode.class);
    
    private final Project project;

    public ProjectNode(Project project) {
        
        /*
         * To make the project-sensitive actions work, we need to put the project
         * into the lookup of the root node; the presence of a directory manager
         * identifies this project as a rules4jbi project.
         */
        super(Children.create(new ProjectNodeChildFactory(project), false),
                Lookups.fixed(project, project.getLookup().lookup(DirectoryManager.class)));
        
        this.project = project;
    }
    
    @Override
    public Image getIcon(int type) {
        return ImageUtilities.loadImage(RULES4JBI_ICON);
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getIcon(type);
    }

    @Override
    public String getDisplayName() {
        return project.getProjectDirectory().getName();
    }

    @Override
    public Action[] getActions(boolean context) {
        if (context) {
            return super.getActions(true);
            
        } else {
            return new Action[] {
                CommonProjectActions.newFileAction(),
                null,
                ProjectSensitiveActions.projectCommandAction(
                        ActionProvider.COMMAND_BUILD,
                        resourceBundle.getString("ProjectNode.build.action.name"),
                        null),
                ProjectSensitiveActions.projectCommandAction(
                        ActionProvider.COMMAND_REBUILD,
                        resourceBundle.getString("ProjectNode.clean.and.build.action.name"),
                        null),
                ProjectSensitiveActions.projectCommandAction(
                        ActionProvider.COMMAND_CLEAN,
                        resourceBundle.getString("ProjectNode.clean.action.name"),
                        null),
                null,
                ActionsFactory.importFileAction(),
                ActionsFactory.compileSchemasAction(),
                ActionsFactory.createWSDLAction(),
                null,
                CommonProjectActions.setAsMainProjectAction(),
                CommonProjectActions.closeProjectAction(),
                null,
                CommonProjectActions.renameProjectAction(),
                CommonProjectActions.moveProjectAction(),
                CommonProjectActions.copyProjectAction(),
                CommonProjectActions.deleteProjectAction(),
                null,
                CommonProjectActions.customizeProjectAction()
            };
        }
    }
}
