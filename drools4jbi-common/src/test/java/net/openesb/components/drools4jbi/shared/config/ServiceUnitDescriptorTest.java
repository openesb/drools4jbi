/*
 * @(#)ServiceUnitDescriptorTest.java        $Revision: 1.2 $ $Date: 2008/07/03 05:46:04 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */

package net.openesb.components.drools4jbi.shared.config;

import nu.xom.Element;
import org.junit.Test;
import net.openesb.components.drools4jbi.shared.util.XOMUtils;

import javax.xml.namespace.QName;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static nu.xom.tests.XOMTestCase.assertEquals;
import static nu.xom.tests.XOMTestCase.assertTrue;

//import static org.junit.Assert.*;

/**
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.2 $ $Date: 2008/07/03 05:46:04 $
 * 
 * @since 0.1
 */
public class ServiceUnitDescriptorTest {

    @Test
    public void createDescriptor() {
        String expected = "<jbi version='1.0' xmlns='http://java.sun.com/xml/ns/jbi' "
                + "xmlns:ns1='http://www.example.com/xml/ns/descriptor'>"
                + "<services binding-component='false'>"
                + "<provides interface-name='ns1:TestPortType' "
                + "service-name='ns1:TestService' "
                + "endpoint-name='TestPort'/></services></jbi>";
        
        Element result = ServiceUnitDescriptor.createDescriptor("http://www.example.com/xml/ns/descriptor",
                                    "TestPortType", "TestService", "TestPort");
        
        assertEquals(XOMUtils.toElement(expected), result);
    }
    
    @Test
    public void load() throws FileNotFoundException, InvalidServiceUnitDescriptorException {
        InputStream descriptor1xmlSteam = ServiceUnitDescriptorTest.class.getResourceAsStream("/descriptor1.xml");

        ServiceUnitDescriptor descriptor1 = ServiceUnitDescriptor.load(descriptor1xmlSteam);
        
        assertEquals(new QName("http://www.example.com/xml/ns/descriptor", "TestPortType"), 
                descriptor1.getInterfaceName());
        
        assertEquals(new QName("http://www.example.com/xml/ns/descriptor", "TestService"),
                descriptor1.getServiceName());
        
        assertEquals("TestPort", descriptor1.getEndpointName());
        
        InputStream descriptor2xmlSteam = ServiceUnitDescriptorTest.class.getResourceAsStream("/descriptor2.xml");

        ServiceUnitDescriptor descriptor2 = ServiceUnitDescriptor.load(descriptor2xmlSteam);
        
        assertEquals(new QName("http://abc.com/jbi", "hello"), descriptor2.getInterfaceName());
        
        assertEquals(new QName("http://abc.com/jbi", "helloService"), descriptor2.getServiceName());
        
        assertEquals("helloendpoint", descriptor2.getEndpointName());
    }
    
    @Test(expected=InvalidServiceUnitDescriptorException.class)
    public void loadException() throws FileNotFoundException, InvalidServiceUnitDescriptorException {
         InputStream descriptor3xmlSteam = ServiceUnitDescriptorTest.class.getResourceAsStream("/descriptor3.xml");

        ServiceUnitDescriptor descriptor3 = ServiceUnitDescriptor.load(descriptor3xmlSteam);
        
        assertTrue(true);
    }
}
