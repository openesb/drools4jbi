/*
 * Copyright 2015 nheron.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.openesb.components.drools4jbi.engine.component;

import java.util.ArrayList;
import java.util.List;
import org.chtijbug.drools.entity.history.HistoryEvent;
import org.chtijbug.drools.runtime.DroolsChtijbugException;
import org.chtijbug.drools.runtime.listener.HistoryListener;

/**
 *
 * @author nheron
 */
public class Drools4JBIHistoryListener implements HistoryListener{

    private List<HistoryEvent> historyEventList = new ArrayList();

    public Drools4JBIHistoryListener() {
    }

    public List<HistoryEvent> getHistoryEventList() {
        return historyEventList;
    }
    
    public void flushHistoryEventList(){
        this.historyEventList.clear();
    }
    
    @Override
    public void fireEvent(HistoryEvent newHistoryEvent) throws DroolsChtijbugException {
        this.historyEventList.add(newHistoryEvent);
    }
    
}
