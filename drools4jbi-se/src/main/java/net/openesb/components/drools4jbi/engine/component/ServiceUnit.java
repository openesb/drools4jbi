/*
 * @(#)ServiceUnit.java        $Revision: 1.10 $ $Date: 2009/01/14 02:53:12 $
 * 
 * Copyright (c) 2008 Milan Fort (http://www.milanfort.com/). All rights reserved.
 * 
 * The contents of this file are subject to the terms of the Common Development
 * and Distribution License (the "License"). You may not use this file except
 * in compliance with the License.
 * 
 * You can obtain a copy of the license at http://www.sun.com/cddl/cddl.html.
 * See the License for the specific language governing permissions and limitations
 * under the License.
 */
package net.openesb.components.drools4jbi.engine.component;

import com.google.inject.Inject;
import net.openesb.components.drools4jbi.engine.guice.annotations.Unit;
import net.openesb.components.drools4jbi.engine.wsdl.JBIWrapper;
import net.openesb.components.drools4jbi.shared.classloader.BusinessObjectsNotFoundException;
import net.openesb.components.drools4jbi.shared.classloader.ClassLoaderFactory;
import net.openesb.components.drools4jbi.shared.config.Configuration;
import net.openesb.components.drools4jbi.shared.config.InvalidConfigurationException;
import net.openesb.components.drools4jbi.shared.config.InvalidServiceUnitDescriptorException;
import net.openesb.components.drools4jbi.shared.config.ServiceUnitDescriptor;
import net.openesb.components.drools4jbi.shared.logging.Logger;
import net.openesb.components.drools4jbi.shared.util.XOMUtils;
import net.openesb.components.drools4jbi.shared.wsdl.WSDLConstants;
import net.openesb.components.drools4jbi.shared.wsdl.extension.ExtensionRegistrySupport;
import nu.xom.*;
import nu.xom.converters.DOMConverter;
import org.chtijbug.drools.runtime.DroolsChtijbugException;
import org.chtijbug.drools.runtime.RuleBaseBuilder;
import org.chtijbug.drools.runtime.RuleBasePackage;
import org.chtijbug.drools.runtime.RuleBaseSession;
import org.drools.ObjectFilter;

import javax.jbi.JBIException;
import javax.jbi.component.ComponentContext;
import javax.jbi.management.DeploymentException;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ExtensionRegistry;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.wsdl.xml.WSDLWriter;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static net.openesb.components.drools4jbi.shared.GlobalConstants.*;

/**
 * This class represents a service unit; a single deployed executable ruleset.
 *
 * @author Milan Fort (http://www.milanfort.com/)
 * @version $Revision: 1.10 $ $Date: 2009/01/14 02:53:12 $
 * @since 0.1
 */
public final class ServiceUnit {

    private static final String SERVICE_UNIT_DESCRIPTOR_LOCATION
            = META_INF_DIR + File.separator + JBI_FILE_NAME;

    /* Injected resources */
    @Inject
    @Unit
    private Logger logger;

    @Inject
    private ComponentContext componentContext;

    private RuleBasePackage ruleBasePackage = null;

    /* User provided resources */
    private String name = null;

    private String rootPath = null;

    /* Computed resources */
    private Configuration configuration;

    private Definition definition;

    private ServiceUnitDescriptor descriptor;

    private ServiceEndpoint serviceEndpoint;

    private volatile boolean initialized = false;

    private volatile boolean activated = false;

    private String targetNamespace = null;
    /**
     * Ordered list of output object element names.
     */
    private List<QName> outputElements;

    private ClassLoader componentClassLoader;

    private  Drools4JBIHistoryListener bIHistoryListener;
    
    /**
     * Objects representing facts used by this service unit.
     */
    private Class<?>[] classes;
    private ClassLoader classLoader;

    /* Instances of this class should be created only by Guice */
    public ServiceUnit() {
    }

    void setName(String name) {
        this.name = name;
    }

    void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public void init() throws DeploymentException {
        if (name == null || rootPath == null) {
            throw new IllegalStateException(
                    "This service unit was not created properly via the ServiceUnitFactory");
        }

        configuration = loadConfiguration(new File(rootPath, CONFIG_FILE_NAME));

        descriptor = loadServiceUnitDescriptor(
                new File(rootPath + File.separator + SERVICE_UNIT_DESCRIPTOR_LOCATION));

        logger.fine("Deployment descriptor for this service unit: %s", descriptor.toString());

        definition = loadWSDL(rootPath + File.separator + configuration.getWSDLFile());

        logger.fine("WSDL target namespace: '%s'", definition.getTargetNamespace());

        List<?> extensibilityElements = definition.getExtensibilityElements();

        for (Object extensibilityElement : extensibilityElements) {
            logger.fine("Found extensibility element: %s", extensibilityElement);
        }

//        Document wsdlDocument = DOMConverter.convert(getServiceDescription());
//        XOMUtils.prettyPrint(wsdlDocument);
        outputElements = parseOutputElements(definition);

        logger.fine("Parsed output elements: %s", outputElements);

        classLoader = initClassLoader(
                new File(rootPath, CLASSES_DIR),
                new File(rootPath, LIBRARIES_DIR),
                new File(rootPath, ENGINE_DIR));

        classes = loadClasses(classLoader, configuration.getClasses());

        initialized = true;
    }

    private Configuration loadConfiguration(File file) throws DeploymentException {
        InputStream inputStream = null;

        try {
            logger.fine("Loading configuration from file '%s'", file.getAbsolutePath());

            inputStream = new FileInputStream(file);

            return Configuration.load(inputStream);

        } catch (FileNotFoundException e) {
            logger.severe("Could not find the configuration file");

            throw new DeploymentException(e);

        } catch (InvalidConfigurationException e) {
            logger.severe("Could not retrieve the configuration file");

            throw new DeploymentException(e);

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();

                } catch (IOException e) {
                    logger.fine("Failed to close the input stream", e);
                }
            }
        }
    }

    private ServiceUnitDescriptor loadServiceUnitDescriptor(File file) throws DeploymentException {
        InputStream inputStream = null;

        try {
            logger.fine("Loading service unit descriptor from file '%s'", file.getAbsolutePath());

            inputStream = new FileInputStream(file);

            return ServiceUnitDescriptor.load(inputStream);

        } catch (FileNotFoundException e) {
            logger.severe("Could not find the service unit descriptor file");

            throw new DeploymentException(e);

        } catch (InvalidServiceUnitDescriptorException e) {
            logger.severe("Could not retrieve the deployment descriptor");

            throw new DeploymentException(e);

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();

                } catch (IOException e) {
                    logger.fine("Failed to close the input stream", e);
                }
            }
        }
    }

    private Definition loadWSDL(String wsdlFilePath) throws DeploymentException {

        try {
            logger.fine("Loading WSDL from file '%s'", wsdlFilePath);

            WSDLFactory wsdlFactory = WSDLFactory.newInstance();

            /* We need to use populated registry, because it contains Schema Extensibility elements */
            ExtensionRegistry registry = wsdlFactory.newPopulatedExtensionRegistry();

            ExtensionRegistrySupport.registerExtensions(registry);

            /*
             * We want the registry to throw an exception if it encounters
             * unknown extensions in the WSDL document
             */
            registry.setDefaultDeserializer(null);
            registry.setDefaultSerializer(null);

            WSDLReader wsdlReader = wsdlFactory.newWSDLReader();

            wsdlReader.setExtensionRegistry(registry);

            /* Do not send anything to the standard output stream */
            wsdlReader.setFeature("javax.wsdl.verbose", false);

            /* The WSDL used to describe this jbi component's services does not import anything */
            wsdlReader.setFeature("javax.wsdl.importDocuments", false);

            return wsdlReader.readWSDL(wsdlFilePath);

        } catch (WSDLException e) {
            logger.severe("Could not read the WSDL document", e);

            throw new DeploymentException(e);
        }
    }

    private List<QName> parseOutputElements(final Definition definition) throws DeploymentException {
        try {
            WSDLFactory factory = WSDLFactory.newInstance();
            WSDLWriter writer = factory.newWSDLWriter();

            Document wsdl = DOMConverter.convert(writer.getDocument(definition));

//            logger.fine(wsdl.toXML());
            XPathContext context = new XPathContext("xs", WSDLConstants.XML_SCHEMA_NAMESPACE_URI);

            Nodes query = wsdl.query("//xs:schema[@targetNamespace='" + WSDLConstants.TYPES_NAMESPACE_URI + "']",
                    context);

            if (query.size() != 1) {
                throw new DeploymentException("Invalid WSDL; could not find the output element");
            }

            Element typesSchemaElement = (Element) query.get(0);

            logger.fine("Found types schema element:");
            logger.finest(typesSchemaElement.toXML());

            query = typesSchemaElement.query("//xs:element[@name='" + WSDLConstants.OUTPUT_ELEMENT_NAME + "']"
                    + "/xs:complexType/xs:sequence/xs:element/@ref", context);

            List<Node> refAttributes = XOMUtils.asList(query);

            logger.fine("Found %d output element references", refAttributes.size());

            List<QName> result = new ArrayList<QName>();

            for (Node node : refAttributes) {
                Attribute refAttribute = (Attribute) node;

                String prefixedElementName = refAttribute.getValue();

                logger.fine("Parsing element reference: %s", prefixedElementName);

                String[] parsedElementName = prefixedElementName.split(":");

                if (parsedElementName.length != 2) {
                    throw new DeploymentException("Invalid WSDL; could not parse output element reference");
                }

                String prefix = parsedElementName[0];
                String localName = parsedElementName[1];

                String namespaceURI = typesSchemaElement.getNamespaceURI(prefix);

                if (namespaceURI == null) {
                    throw new DeploymentException(
                            "Invalid WSDL; could not find namespace uri of a referenced element");
                }

                logger.fine("Namespace uri corresponding to prefix '%s' is '%s'", prefix, namespaceURI);

                QName elementQualifiedName = new QName(namespaceURI, localName);

                logger.fine("Successfully parsed output element: %s", elementQualifiedName);

                result.add(elementQualifiedName);
            }

            return Collections.unmodifiableList(result);

        } catch (WSDLException e) {
            logger.severe("Unable to parse service description", e);

            throw new DeploymentException(e);

        } catch (Exception e) {
            logger.severe("Unknown error occurred while parsing the output elements", e);

            throw new DeploymentException(e);
        }
    }

    private ClassLoader initClassLoader(final File classesDir, final File libDir, final File engineDir)
            throws DeploymentException {
        try {
            logger.fine("Initializing class loader");

            logger.fine("Classes directory URL: '%s'", classesDir.toURI().toURL().toString());
            logger.fine("Libraries directory URL: '%s'", libDir.toURI().toURL().toString());
            logger.fine("Rules Engine directory URL: '%s'", engineDir.toURI().toURL().toString());

            return ClassLoaderFactory.createServiceUnitClassLoader(
                    classesDir, libDir, engineDir, componentClassLoader);

        } catch (MalformedURLException e) {
            logger.severe("Failed to initialize the classloader");

            throw new DeploymentException(e);

        } catch (BusinessObjectsNotFoundException e) {
            logger.severe("Failed to initialize the classloader; no business objects found");

            throw new DeploymentException(e);
        }
    }

    private Class<?>[] loadClasses(ClassLoader classLoader, List<String> classNames) throws DeploymentException {
        logger.fine("Loading classes: %s", classNames);

        Class<?>[] result = new Class<?>[classNames.size()];

        for (int i = 0; i < result.length; i++) {
            String className = classNames.get(i);

            try {
                result[i] = Class.forName(className, true, classLoader);

                logger.fine("Successfully loaded class '%s'", className);

            } catch (ClassNotFoundException e) {
                logger.severe("Failed to load class '%s'", className);

                throw new DeploymentException(e);
            }
        }

        return result;
    }

    public void start() throws DeploymentException {
        logger.entering(this.getClass(), "start");

        if (!initialized) {
            throw new IllegalStateException("This service unit is not yet initialized");
        }

        try {
            serviceEndpoint
                    = componentContext.activateEndpoint(descriptor.getServiceName(), descriptor.getEndpointName());
            activated = true;

        } catch (JBIException e) {
            throw new DeploymentException(e);
        }
    }

    public void stop() throws DeploymentException {
        if (!activated) {
            throw new IllegalStateException("This service unit was not yet activated");
        }

        try {
            componentContext.deactivateEndpoint(serviceEndpoint);
            activated = false;

        } catch (JBIException e) {
            throw new DeploymentException(e);
        }
    }

    public void shutDown() {
        initialized = false;
    }

    public void process(final InOut messageExchange) {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        ClassLoader moduleCl = this.classLoader;

        RuleBaseSession droolsSession = null;

        try {
            Thread.currentThread().setContextClassLoader(moduleCl);
            if (ruleBasePackage == null) {
                bIHistoryListener = new Drools4JBIHistoryListener();
                if (configuration.getRuleflowFile() != null && configuration.getRuleflowFile().length() > 0) {
                    ruleBasePackage = RuleBaseBuilder.createPackageBasePackageWithListenerandRootPath(rootPath, bIHistoryListener,  configuration.getRulesetFile(),configuration.getRuleflowFile());

                } else {
                    ruleBasePackage = RuleBaseBuilder.createPackageBasePackageWithListenerandRootPath(rootPath, bIHistoryListener, configuration.getRulesetFile());

                }
            }
            /**
             * Collection<Object> toto = this.knowledgeSession.getObjects(new
             * ObjectFilter() {
             *
             * @Override public boolean accept(Object o) { return false; } });
             */
            this.bIHistoryListener.flushHistoryEventList();
            droolsSession = ruleBasePackage.createRuleBaseSession();

            logger.fine("Statefull rule session opened: " + droolsSession);

            NormalizedMessage inMessage = messageExchange.getInMessage();
            Source inputSource = inMessage.getContent();

            logger.fine("Converting the Source to nu.xom.Document");
            Document document = XOMUtils.toDocument(inputSource);
            logger.fine("Document element: %s", document.toXML());

            Element message = document.getRootElement();
            logger.fine("Message element: %s", message.toXML());

            Element part = message.getFirstChildElement("part", "http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper");
            logger.fine("Part element: %s", part.toXML());

            Element data = part.getFirstChildElement(WSDLConstants.INPUT_ELEMENT_NAME, WSDLConstants.TYPES_NAMESPACE_URI);
            logger.fine("Data element: %s", data.toXML());

            List<nu.xom.Element> businessObjectElements = XOMUtils.asList(data.getChildElements());
            List<Object> businessObjects = new ArrayList<Object>();

            Serializer serializer = new Serializer(classes);

            for (Element element : businessObjectElements) {
                logger.fine("Business object xml: %s", element.toXML());

                Object obj = serializer.deserialize(element);
                logger.fine("Business object: %s", obj);

                businessObjects.add(obj);
            }

            logger.fine("Executing rules with input: %s", businessObjects);
            for (Object anObject : businessObjects) {
                try {
                    droolsSession.insertByReflection(anObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (configuration.getRuleflowFile() != null
                    && configuration.getRuleflowFile().length() > 0
                    && configuration.getProcessName() != null
                    && configuration.getProcessName().length() > 0) {
                droolsSession.startProcess(configuration.getProcessName());
            }
            droolsSession.fireAllRules();

            final QNameObjectFilter filter = new QNameObjectFilter(outputElements);
            Collection<Object> result = droolsSession.getObjects(new ObjectFilter() {
                @Override
                public boolean accept(Object o) {
                    if (filter.filter(o) != null) {
                        return true;
                    }
                    return false;
                }
            });

            logger.fine("Rule execution returned: %s", result);
            List<Object> result2 = new ArrayList<>();
            result2.add(result);
            Collections.sort(result2, new QNameComparator(outputElements));

            JBIWrapper outputDataWrapper = new JBIWrapper(JBIWrapper.Type.OUTPUT, definition.getTargetNamespace());
            for (Object obj : result) {
                logger.fine("Resulting business object: %s", obj);
                outputDataWrapper.addBusinessObject(serializer.serialize(obj));
            }
            logger.fine("Sending result %s", outputDataWrapper.getWrapper().toXML());
            Source outputSource = outputDataWrapper.toDOMSource();

            NormalizedMessage outMessage = messageExchange.createMessage();
            outMessage.setContent(outputSource);
            messageExchange.setOutMessage(outMessage);
            droolsSession.dispose();
            droolsSession = null;

        } catch (MessagingException e) {
            logger.fine("Could not send the message", e);
        } catch (DroolsChtijbugException e) {
            logger.fine("Drools Error", e);
        } finally {
            if (droolsSession != null) {
                try {
                    droolsSession.dispose();

                    logger.fine("Session released successfully");

                } catch (Exception e) {
                    logger.fine("Failed to release the session", e);
                }
            }
            Thread.currentThread().setContextClassLoader(cl);
        }

    }

    public org.w3c.dom.Document getServiceDescription() {
        try {

            WSDLFactory factory = WSDLFactory.newInstance();
            WSDLWriter writer = factory.newWSDLWriter();

            return writer.getDocument(definition);

        } catch (WSDLException e) {
            logger.severe("Unable to create service description", e);

            return null;
        }
    }

    public ServiceEndpoint getServiceEndpoint() {

        return descriptor.getServiceEndpoint();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Service Unit '");
        sb.append(name);
        sb.append("'\n");
        sb.append("root-path: ");
        sb.append(rootPath);
        sb.append("\n");

        if (initialized) {
            sb.append(configuration);
            sb.append("\n");
            sb.append(descriptor);
            sb.append("\n");
            sb.append("wsdl-name: ");
            sb.append(definition.getQName());
            sb.append("\n");
        }

        return sb.toString();
    }

    void setComponentClassLoader(ClassLoader componentClassLoader) {
        this.componentClassLoader = componentClassLoader;
    }
}
